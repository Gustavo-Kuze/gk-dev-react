import './css/About.css';
import React from 'react';

export default class About extends React.Component {
  render() {
    return (
      <div className="column is-full">
        <section className="gradient-section gradient-section-adjust-edges gradient-section-adjust-size">
          <div className="columns is-multiline">
            <div className="column is-hidden-mobile parallax parallax-adjust-size parallax-adjust-edges about-parallax"></div>
            <div className="column">
              <div className="">
                <h1 className="title made-by-hand-title">
                  Feito à mão{' '}
                  <span
                    className="is-size-3"
                    role="img"
                    aria-label="Emoji de Braço forte"
                  >
                    💪
                  </span>
                </h1>
                <p className="subtitle has-text-centered"></p>
                <div className="columns is-multiline">
                  <div className="column is-narrow is-10-mobile is-8-tablet is-9-desktop is-offset-1-mobile is-offset-2-tablet is-offset-2-desktop">
                    <div className="card table-card">
                      <div className="card-header">
                        <p className="card-header-title">
                          Tecnologias usadas neste site:
                        </p>
                      </div>
                      <div className="card-content ">
                        <div className="table-container">
                          <table className="table is-small is-narrow is-mobile is-offset-one-quarter-mobile has-text-teal">
                            <thead>
                              <tr>
                                <th>Estilo</th>
                                <th>Comportamento</th>
                                <th>Produtividade</th>
                                <th>Desenvolvimento</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td>
                                  <a
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    href="https://bulma.io/"
                                  >
                                    Bulma
                                  </a>
                                </td>
                                <td>
                                  <a
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    href="https://reactjs.org/"
                                  >
                                    React.js
                                  </a>
                                </td>
                                <td>
                                  <a
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    href="https://momentjs.com/"
                                  >
                                    Moment.js
                                  </a>
                                </td>
                                <td>
                                  <a
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    href="https://code.visualstudio.com/"
                                  >
                                    Visual Studio Code
                                  </a>
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <a
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    href="https://react-spring.surge.sh/"
                                  >
                                    React Spring
                                  </a>
                                </td>
                                <td>
                                  <a
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    href="https://www.npmjs.com/package/react-share"
                                  >
                                    React Share
                                  </a>
                                </td>
                                <td>
                                  <a
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    href="https://www.npmjs.com/package/html2plaintext"
                                  >
                                    Html2PlainText
                                  </a>
                                </td>
                                <td>
                                  <a
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    href="https://webpack.js.org/"
                                  >
                                    Webpack
                                  </a>
                                </td>
                              </tr>
                              <tr>
                                <td>
                                  <a
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    href="https://origin.fontawesome.com/"
                                  >
                                    Font-Awesome
                                  </a>
                                </td>
                                <td>
                                  <a
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    href="https://github.com/mattboldt/typed.js/"
                                  >
                                    Typed.js
                                  </a>
                                </td>
                                <td>
                                  <a
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    href="https://highlightjs.org/"
                                  >
                                    Highlight.js
                                  </a>
                                </td>
                                <td>
                                  <a
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    href="https://www.npmjs.com/package/node-sass"
                                  >
                                    Node-Sass
                                  </a>
                                </td>
                              </tr>
                              <tr>
                                <td></td>
                                <td></td>
                                <td>
                                  <a
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    href="https://wordpress.com/"
                                  >
                                    Wordpress
                                  </a>
                                </td>
                                <td>
                                  <a
                                    target="_blank"
                                    rel="noopener noreferrer"
                                    href="https://www.netlify.com/docs/cli/"
                                  >
                                    Netlify
                                  </a>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
