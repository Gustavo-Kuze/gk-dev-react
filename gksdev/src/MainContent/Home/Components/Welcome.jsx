import React from 'react';
import Typed from 'typed.js';
import { Spring, animated } from 'react-spring';

export default class Welcome extends React.Component {
  initTypedJs = () => {
    var options = {
      strings: [
        'Desenvolvimento Web',
        'Desenvolvimento Web',
        'Desenvolvimento de APIs REST',
        'Desenvolvimento de sites com ReactJS',
        'Desenvolvimento de apps com React Native',
        'Suporte a sistemas',
      ],
      typeSpeed: 60,
      backSpeed: 30,
      backDelay: 2500,
      loop: true,
      smartBackspace: true,
    };

    new Typed('#main-welcome-title', options);
  };

  getJorneyDuration = () => {
    return new Date(Date.now()).getFullYear() - 2015;
  };

  componentDidMount = () => {
    this.initTypedJs();
  };

  render() {
    return (
      <section className="section" style={{ marginTop: '50px' }}>
        <div className="container is-fluid">
          <div className="columns is-multiline">
            <div className="column is-half is-offset-one-quarter">
              <h1 id="main-welcome-title" className="subtitle is-3 is-inline">
                {' '}
              </h1>
              <div className="columns is-multiline">
                <div
                  className=""
                  style={{
                    display: 'flex',
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                >
                  <Spring
                    native
                    from={{
                      opacity: '-0.9',
                      transform: 'translate(0px, 150px)',
                    }}
                    to={{ opacity: '1', transform: 'translate(0px, 0px)' }}
                    config={{ duration: 800 }}
                  >
                    {props => (
                      <animated.div style={props}>
                        <figure className="image" style={{ width: '175px' }}>
                          <img
                            className="is-rounded"
                            src="/img/profile.jpg"
                            alt="Logo"
                            style={{marginTop: 50, marginBottom: 40, boxShadow: '3px 3px 15px #111'}}
                          />
                        </figure>
                      </animated.div>
                    )}
                  </Spring>
                </div>
              </div>
              <p className="is-size-4">
                Olá, eu me chamo Gustavo, sou estudante de Análise e
                Desenvolvimento de Sistemas e um aficionado pelo mundo da
                programação!
              </p>
              <p className="is-size-4">
                Em minha jornada de estudos, que já dura{' '}
                {this.getJorneyDuration()} anos, tenho estudado diversas
                tecnologias como:
              </p>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
