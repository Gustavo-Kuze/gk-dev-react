/* eslint-disable react/prefer-stateless-function */
import React, { Component } from 'react';
import PageTemplate from '../../../Base/Components/PageTemplate';
import MyWorks from '../MyWorks';
import Welcome from '../Welcome';
import Techs from '../Technologies';
import About from '../About';
import Code from '../Code';
import { techs } from './techs';

export default class Home extends Component {
  render() {
    return (
      <>
        <PageTemplate>
          <div className="columns is-0 is-mobile is-multiline is-centered">
            <div className="column ">
              <Welcome />
              <div className="container">
                <div className="columns is-multiline is-centered is-mobile">
                  <div className="column is-8-mobile is-6-tablet is-7-desktop ">
                    <Techs elems={techs} big />
                  </div>
                </div>
              </div>
              <p
                className="is-size-4"
                style={{ marginTop: '175px', marginBottom: '100px' }}
              >
                Confira um pouco do meu trabalho <i className="fas fa-coffee" />
              </p>
              <MyWorks />
            </div>
            <About />
            <Code />
          </div>
        </PageTemplate>
      </>
    );
  }
}
