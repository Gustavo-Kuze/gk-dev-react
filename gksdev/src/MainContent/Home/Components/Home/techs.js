const techs = [
  { name: 'Javascript (ES6/ES7)', color: 'warning' },
  { name: 'Bootstrap 3 e 4', color: 'danger' },
  { name: 'HTML5', color: 'danger' },
  { name: 'Jquery', color: 'info' },
  { name: 'MySQL', color: 'success' },
  { name: 'React router', color: 'info' },
  { name: 'MongoDB', color: 'success' },
  { name: 'Microsoft SQL Server', color: 'danger' },
  { name: 'Regex', color: 'link' },
  { name: 'React.js', color: 'info' },
  { name: 'NodeJS', color: 'success' },
  { name: 'Express.js', color: 'warning' },
  { name: 'C#', color: 'danger' },
  { name: 'Postman', color: 'link' },
  { name: 'CSS3', color: 'warning' },
  { name: 'ASP.NET Core', color: 'danger' },
  { name: 'Bulma', color: 'secondary' },
  { name: 'Sass', color: 'primary' },
  { name: 'Styled-components', color: 'primary' },
  { name: 'React-DVA', color: 'primary' },
];

export { techs };
