const works = [
  {
    projectLink: 'https://transenzomunck.com.br',
    title: 'TransEnzo - Munck',
    img: '/img/transenzo.png',
    description:
      'Landing page criada para uma empresa de serviços e aluguel de caminhões guindaste (munck)',
    techs: [
      { name: 'Bootstrap 4', color: 'info' },
      { name: 'Jquery', color: 'danger' },
      { name: '2020', color: 'light' },
    ],
  },
  {
    projectLink: 'https://www.pslag.com.br',
    title: 'PS-LAG - Informática',
    img: '/img/pslag.jpg',
    description: 'Assistência Técnica de Informática especializada.',
    techs: [
      { name: 'Jquery', color: 'info' },
      { name: 'Bootstrap 4', color: 'danger' },
      { name: 'Wordpress', color: 'info' },
      { name: '2017', color: 'light' },
    ],
  },
  {
    projectLink: 'https://randomizador.com.br',
    sourceCodeLink: 'https://github.com/Gustavo-Kuze/randomizador',
    title: 'Randomizador',
    img: '/img/randomizador.JPG',
    description:
      'Sorteios de comentários do Facebook, Listas personalizáveis e muitas outras ferramentas de sorteio!',
    techs: [
      { name: 'React', color: 'info' },
      { name: 'Redux', color: 'success' },
      { name: 'Firebase', color: 'warning' },
      { name: 'Firestore', color: 'warning' },
      { name: 'Netlify', color: 'success' },
      { name: 'Bootstrap 4', color: 'danger' },
      { name: 'Redux Saga', color: 'success' },
      { name: 'Login do Google', color: 'warning' },
      { name: 'Login do Facebook', color: 'info' },
      { name: 'Facebook Graph API', color: 'info' },
      { name: '2019', color: 'light' },
    ],
  },
  {
    projectLink: 'https://bilhetinhos.netlify.com/',
    sourceCodeLink: 'https://github.com/Gustavo-Kuze/Bilhetinhos',
    title: 'Bilhetinhos',
    img: '/img/bilhetinhos.jpg',
    description:
      'Uma rede social minimalista feita com React, Redux e Firebase',
    techs: [
      { name: 'React', color: 'info' },
      { name: 'Redux', color: 'success' },
      { name: 'Firebase', color: 'warning' },
      { name: 'Bootstrap 4', color: 'danger' },
      { name: 'Login do Google', color: 'warning' },
      { name: '2018', color: 'light' },
    ],
  },
  {
    projectLink: '/website-templates/tl/index.html',
    sourceCodeLink:
      'https://bitbucket.org/Gustavo-Kuze/gk-dev-react/src/7b63bfd1cc636e1f6d70883ec4c753bd49f88de2/gksdev/public/website-templates/tl/?at=master',
    title: 'Traçando Livros',
    img: '/img/tracando-livros.jpg',
    description:
      'Um website para amantes da leitura. Um livro virtual feito inteiramente com elementos da página.',
    techs: [
      { name: 'Jquery', color: 'info' },
      { name: 'HTML 5', color: 'danger' },
      { name: 'Turn.js', color: 'success' },
      { name: 'Bootstrap 4', color: 'danger' },
      { name: '2017', color: 'light' },
    ],
  },
  {
    projectLink: '/website-templates/arambare/index.html',
    sourceCodeLink:
      'https://bitbucket.org/Gustavo-Kuze/gk-dev-react/src/7b63bfd1cc636e1f6d70883ec4c753bd49f88de2/gksdev/public/website-templates/arambare/?at=master',
    title: 'Capital das figueiras',
    img: '/img/arambare.jpg',
    description:
      'Um template inspirado em Arambaré, um dos mais belos refúgios para aqueles que precisam dar um tempo da correria da cidade',
    techs: [
      { name: 'Jquery', color: 'info' },
      { name: 'Bootstrap 4', color: 'danger' },
      { name: 'Google maps', color: 'warning' },
      { name: '2018', color: 'light' },
    ],
  },
  {
    projectLink: '/website-templates/multiractjs/index.html',
    sourceCodeLink:
      'https://bitbucket.org/Gustavo-Kuze/gk-dev-react/src/7b63bfd1cc636e1f6d70883ec4c753bd49f88de2/gksdev/public/website-templates/multiractjs/?at=master',
    title: 'Multiract.js',
    img: '/img/multiractjs.jpg',
    description:
      'Transcreva textos de imagens em mais de 60 idiomas. Demonstração do motor de reconhecimento de caracteres Teserract.',
    techs: [
      { name: 'Jquery', color: 'info' },
      { name: 'Bootstrap 4', color: 'danger' },
      { name: 'Tesseract.js', color: 'warning' },
      { name: '2017', color: 'light' },
    ],
  },
];

export { works };
