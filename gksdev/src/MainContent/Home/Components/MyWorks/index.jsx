import React from 'react';
import Work from './Work';
import { works } from './works';

const MyWorks = () => {
  const createWork = (
    projectLink,
    sourceCodeLink,
    title,
    img,
    description,
    techs,
  ) => {
    return (
      <Work
        key={title}
        projectLink={projectLink}
        sourceCodeLink={sourceCodeLink}
        title={title}
        img={img}
        description={description}
        techs={techs}
      />
    );
  };

  return (
    <section className="section">
      <div className="container ">
        <div className="columns is-multiline">
          {works.map(w =>
            createWork(
              w.projectLink,
              w.sourceCodeLink,
              w.title,
              w.img,
              w.description,
              w.techs,
            ),
          )}
        </div>
      </div>
    </section>
  );
};

export default MyWorks;
