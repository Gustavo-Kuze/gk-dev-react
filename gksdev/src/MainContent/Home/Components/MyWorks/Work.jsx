import '../../../Base/Components/css/FlexCard.css';
import React from 'react';
import Techs from '../Technologies';

export default props => (
  <div className="column is-4">
    <div className="card flex-card has-background-dark has-text-white">
      <header className="card-header">
        <p className="card-header-title has-text-white ">{props.title}</p>
      </header>
      <div className="card-content flex-card-content">
        <div className="columns is-centered is-mobile">
          <div className="column">
            {props.description}
            <br />
            <img
              src={props.img}
              alt={props.imgAlt}
              style={{ padding: '4px' }}
            />
            <br />
            <Techs elems={props.techs} big={false} />
          </div>
        </div>
      </div>
      <footer className="card-footer flex-card-footer">
        <a
          target="_blank"
          rel="noopener noreferrer"
          href={props.projectLink || '#'}
          className="card-footer-item button is-link"
        >
          <i className="fas fa-eye"></i>
        </a>
        {props.sourceCodeLink ? (
          <a
            target="_blank"
            rel="noopener noreferrer"
            href={props.sourceCodeLink}
            className="card-footer-item button is-dark"
          >
            <i className="fas fa-code"> </i>
          </a>
        ) : (
          <br />
        )}
      </footer>
    </div>
  </div>
);
