import React, { Component } from 'react';

export default class Techs extends Component {
  render() {
    return (
      <div className="field is-grouped-multiline">
        <div className="control">
          <div className="tags">
            {this.props.elems
              ? this.props.elems.map(elem => (
                  <span
                    key={elem.name}
                    className={`tag ${this.props.big ? 'is-medium' : ''} is-${
                      this.props.big ? 'dark' : elem.color
                    }`}
                  >
                    {elem.name}
                  </span>
                ))
              : ''}
          </div>
        </div>
      </div>
    );
  }
}
