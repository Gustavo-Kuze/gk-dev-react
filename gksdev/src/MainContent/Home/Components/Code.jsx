import './css/Code.css';
import React, { Component } from 'react';

export default class Code extends Component {
  render() {
    return (
      <div className="column is-12">
        <section>
          <div className="columns is-centered is-multiline">
            <div className="column">
              <div className="show-me-the-code-container">
                <h1 className="title">Falar é fácil, mostre-me o código!</h1>
                <p className="subtitle">
                  O código fonte deste website pode ser encontrado em meu{' '}
                  <a
                    href="https://bitbucket.org/Gustavo-Kuze/gk-dev-react/src/master/"
                    target="_blank"
                    rel="noopener noreferrer"
                    className="has-text-primary"
                  >
                    Bitbucket
                  </a>{' '}
                </p>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
