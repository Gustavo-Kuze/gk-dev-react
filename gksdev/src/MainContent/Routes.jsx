import React from 'react';
import { Switch, Route, Redirect } from 'react-router';
import Home from './Home/Components/Home/';
import Blog from './Blog/Components/Blog';

export default () => (
  <Switch>
    <Route exact path="/" component={Home} />
    <Redirect exact from="/blog/" to="/blog/1" />
    <Route path="/blog/:id(\d+)" component={Blog} />
    <Redirect from="*" to="/" />
  </Switch>
);
