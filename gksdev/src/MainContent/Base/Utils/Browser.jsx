const getCurrentPage = () => {
  // eslint-disable-next-line radix
  const curPage = parseInt(window.location.pathname.split('/')[2]);
  return curPage;
};

const getPathNamePartsLength = () => window.location.pathname.split('/').length;

const getDecodedSearchParameters = () => {
  const queryString = window.location.search.replace('?', '');
  const queryStringParams = queryString.split('&');

  const searchParameters = {};
  queryStringParams.forEach(param => {
    const key = param.split('=')[0];
    const values = param.split('=')[1];

    searchParameters[`${key}`] = decodeURIComponent(values);
  });

  return searchParameters;
};

const getBlogUrlOnly = () =>
  `${window.location.protocol}//${window.location.host}
  ${window.location.pathname}`;

export {
  getCurrentPage,
  getPathNamePartsLength,
  getDecodedSearchParameters,
  getBlogUrlOnly,
};
