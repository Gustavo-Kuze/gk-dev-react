import React, { Component } from 'react';

export default class Header extends Component {
  componentDidMount() {
    this.initNavBurgers();
  }

  initNavBurgers = () => {
    const $navbarBurgers = Array.prototype.slice.call(
      document.querySelectorAll('.navbar-burger'),
      0,
    );
    if ($navbarBurgers.length > 0) {
      $navbarBurgers.forEach(el => {
        el.addEventListener('click', () => {
          const { target } = el.dataset;
          const $target = document.getElementById(target);
          el.classList.toggle('is-active');
          $target.classList.toggle('is-active');
        });
      });
    }
  };

  render() {
    return (
      <header className="header">
        <div className="container">
          <nav
            className="navbar is-spaced is-fixed-top has-background-dark"
            role="navigation"
            aria-label="main navigation"
          >
            <div className="navbar-brand">
              <a className="navbar-item" href="/">
                <img
                  className="image"
                  src="/img/logo_preto_vermelho_transparent.png"
                  alt="Logo"
                />
              </a>
              <button
                className="button is-dark navbar-burger burger"
                aria-label="menu"
                aria-expanded="false"
                data-target="navbarBasicExample"
                type="button"
              >
                <span aria-hidden="true" />
                <span aria-hidden="true" />
                <span aria-hidden="true" />
              </button>
            </div>
            <div id="navbarBasicExample" className="navbar-menu">
              <div className="navbar-start">
                <a className="navbar-item" href="/">
                  Início
                </a>
                <a className="navbar-item" href="/blog/1">
                  Blog
                </a>
              </div>
            </div>
          </nav>
        </div>
      </header>
    );
  }
}
