/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import './css/ModalMessage.css';
import React from 'react';

export default class ModalMessage extends React.Component {
  close = () => {
    const { onClose } = this.props;
    const modal = document.querySelectorAll('[modal-message]')[0];
    modal.classList.remove('is-active');
    if (onClose) {
      onClose();
    }
  };

  callOkAndClose = () => {
    const { onOk } = this.props;
    onOk();
    this.close();
  };

  handleKeyUp = e => {
    if (e.key === 'Enter') {
      this.callOkAndClose();
    } else if (e.key === 'Escape') {
      this.close();
    }
  };

  render() {
    const { children, title } = this.props;
    return (
      <div modal-message="true" className="modal ">
        <div className="modal-background" onClick={this.close} />
        <div className="modal-card modal-message has-text-light">
          <header className="modal-card-head has-text-light">
            <p className="modal-card-title has-text-light">{title}</p>
          </header>
          <section
            className="modal-card-body has-text-light"
            onKeyUp={this.handleKeyUp}
          >
            {children}
          </section>
          <footer className="modal-card-foot">
            <button
              className="button is-medium has-background-primary has-text-light search-btn-ok"
              aria-label="close"
              onClick={this.callOkAndClose}
              type="button"
            >
              Ok
            </button>
          </footer>
          <button
            className="modal-close is-large has-background-primary "
            aria-label="close"
            onClick={this.close}
            type="button"
          />
        </div>
      </div>
    );
  }
}
