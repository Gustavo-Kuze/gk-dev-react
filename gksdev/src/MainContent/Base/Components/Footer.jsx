import './css/Footer.css';
import React from 'react';

export default function Footer() {
  return (
    <footer className="footer has-background-dark ">
      <div className="content">
        <div className="columns is-multiline">
          <div className="column">
            <h3 className="is-size-2 has-text-light footer-title">
              Entre em contato
            </h3>
            <h4 className="is-size-5 has-text-light footer-subtitle">
              Vamos trocar uma ideia!
            </h4>
          </div>
        </div>
        <div className="columns is-multiline is-vcentered">
          <div className="column">
            <p className="has-text-centered">
              <span className="footer-menu-icon">
                <a
                  href="mailto:gustavoksilva@gmail.com"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <i className="is-size-3 far fa-envelope" />
                </a>
              </span>
              <span className="footer-menu-icon">
                <a
                  href="https://www.linkedin.com/in/gustavo-kuze/"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <i className="is-size-3 fab fa-linkedin" />
                </a>
              </span>
              <span className="footer-menu-icon">
                <a
                  href="https://github.com/Gustavo-Kuze"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <i className="is-size-3 fab fa-github" />
                </a>
              </span>
            </p>
            <p className="footer-menu-copyright">
              Copyright - Gustavo Kuze 2018
            </p>
          </div>
        </div>
      </div>
    </footer>
  );
}
