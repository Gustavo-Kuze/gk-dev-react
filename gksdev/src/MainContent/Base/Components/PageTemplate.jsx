import './css/PageTemplate.css';
import React from 'react';
import Header from './Header';
import Footer from './Footer';
import GoToTopButton from './GoToTopButton';

export default function PageTemplate(props) {
  const { children } = props;
  return (
    <section className="hero is-fullheight-with-navbar is-dark is-bold  ">
      <div className="hero-head ">
        <Header />
      </div>
      <div className="hero-body ">{children}</div>
      <div className="hero-foot">
        <Footer />
      </div>
      <GoToTopButton />
    </section>
  );
}
