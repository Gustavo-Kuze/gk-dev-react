import React from 'react';
import If from './If';

export default ({ load }) => (
  <If condition={load}>
    <div className="column  ">
      <div className="columns is-mobile is-centered">
        <div className="column">
          <div className="linear-activity">
            <div className="indeterminate" />
          </div>
        </div>
      </div>
    </div>
  </If>
);
