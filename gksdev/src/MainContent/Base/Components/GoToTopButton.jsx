import React, { Component } from 'react';
import { Transition } from 'react-spring';

class GoToTopButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showButton: false,
    };
  }

  componentDidMount() {
    window.addEventListener('scroll', () => {
      this.setState({ showButton: window.scrollY > 200 });
    });
  }

  render() {
    const { showButton } = this.state;

    return (
      <Transition
        items={showButton}
        from={{ right: '-500px' }}
        leave={{ right: '-500px' }}
        enter={{ right: '15px' }}
        config={{ duration: 400 }}
      >
        {show =>
          show &&
          (props => (
            <div style={props}>
              <button
                className="button is-primary is-rounded btn-go-to-top"
                style={{
                  position: 'fixed',
                  right: props.right,
                  bottom: '25px',
                  border: '1px solid #f5f5f5 !important',
                }}
                onClick={() => {
                  window.scrollTo({ top: '0', behavior: 'smooth' });
                }}
                type="button"
              >
                <i className="fas fa-arrow-up" />
              </button>
            </div>
          ))
        }
      </Transition>
    );
  }
}

export default GoToTopButton;
