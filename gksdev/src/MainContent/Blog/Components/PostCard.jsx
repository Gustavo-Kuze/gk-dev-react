import './css/PostCard.css';
import { Link } from 'react-router-dom';
import React, { Component } from 'react';
import h2p from 'html2plaintext';
import { formatResponseDate } from '../Utils/AfterResponse';
import { Transition, animated } from 'react-spring';

export default class PostCard extends Component {
  render() {
    return (
      <div className="column is-6-tablet is-4-desktop">
        <Transition
          native
          items={this.props.show}
          from={{ opacity: 0 }}
          enter={{ opacity: 1 }}
          leave={{ opacity: 0 }}
          config={{ duration: 300, delay: this.props.delay || 300 }}
        >
          {show =>
            show &&
            (props => (
              <animated.div style={props}>
                <Link
                  to={this.props.href}
                  onClick={this.props.handlePostClick}
                  className="post-card-link"
                >
                  <div className="card has-background-dark ">
                    <div className="card-image">
                      <figure className="image is-4by3">
                        <img
                          src={
                            this.props.img ||
                            'https://phadvocates.org/wp-content/themes/cardinal/images/default-thumb.png'
                          }
                          alt="Thumbnail"
                        />
                      </figure>
                    </div>
                    <div className="card-content">
                      <div className="media">
                        <div className="media-content">
                          <p className="title is-5 has-text-light has-text-center">
                            {h2p(this.props.title)}
                          </p>

                          <time
                            id={`time-${this.props.slug}`}
                            className="has-text-info"
                          >
                            {formatResponseDate(this.props.date)}
                          </time>
                        </div>
                      </div>
                      <div className="content">
                        <p className="subtitle is-6 has-text-light has-text-justified crop-text">
                          {h2p(this.props.excerpt)}
                        </p>
                      </div>
                    </div>
                  </div>
                </Link>
              </animated.div>
            ))
          }
        </Transition>
      </div>
    );
  }
}
