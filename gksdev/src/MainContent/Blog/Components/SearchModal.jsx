import './css/SearchModal.css';
import React from 'react';
import ModalMessage from '../../Base/Components/ModalMessage';
import { SELECT_CATEGORY_DEFAULT } from '../Utils/Consts';

export default props => {
  return (
    <ModalMessage title="Filtros e pesquisa" onOk={props.handleOk}>
      <div className="columns is-centered">
        <div className="column is-1-tablet" />
        <div className="column is-10-tablet">
          <div className="field">
            <p className=" has-text-light">Buscar posts com estes termos</p>
            <div className="columns is-mobile">
              <div className="column is-10">
                <div className="control">
                  <input
                    className="input search-input"
                    type="text"
                    onChange={props.handleSearchChange}
                    value={props.search}
                  />
                </div>
              </div>
              <div className="column is-2">
                <button
                  className="button is-inverted is-primary search-btn-trash"
                  title="Limpar pesquisa"
                  onClick={props.clearSearch}
                  type="button"
                >
                  <i className="fas fa-trash" />
                </button>
              </div>
            </div>
            <br />
            <p className=" has-text-light">Filtro de categoria</p>
            <div className="control">
              <div className="select is-primary is-fullwidth">
                <select
                  onChange={props.handleCategoryChange}
                  value={props.category}
                  className="search-select"
                >
                  <option>{SELECT_CATEGORY_DEFAULT}</option>
                  {props.categories.map(category => (
                    <option key={category.name}>{category.name}</option>
                  ))}
                </select>
              </div>
            </div>
          </div>
        </div>
        <div className="column is-1-tablet" />
      </div>
    </ModalMessage>
  );
};
