import React, { Component } from 'react';
import {
  FacebookShareButton,
  FacebookIcon,
  LinkedinShareButton,
  LinkedinIcon,
} from 'react-share';

export default class ShareButtons extends Component {
  render() {
    return (
      <div className="columns is-centered is-multiline is-mobile">
        <div
          className="column is-half is-one-quarter"
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <FacebookShareButton
            quote={this.props.title}
            url={`${this.props.URL}`}
            children={<FacebookIcon size={50} round={true} />}
            style={{ marginRight: '5px' }}
          />
          <LinkedinShareButton
            description={this.props.description}
            title={this.props.title}
            url={`${this.props.URL}`}
            children={<LinkedinIcon size={50} round={true} />}
          />
        </div>
      </div>
    );
  }
}
