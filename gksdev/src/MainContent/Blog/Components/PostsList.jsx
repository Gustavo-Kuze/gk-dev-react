import React, { Component } from 'react';
import PostCard from './PostCard';

let currentDelay = 0;

export default class PostsList extends Component {
  componentDidUpdate() {
    currentDelay = 0;
  }

  renderCards() {
    return (
      <React.Fragment>
        {this.props.posts.map(p => (
          <PostCard
            key={p.slug}
            slug={p.slug}
            img={p.featured_image}
            title={p.title}
            date={p.modified}
            excerpt={p.excerpt}
            href={`/blog/${this.props.currentPage}/${p.slug}`}
            handlePostClick={() => this.props.handlePostClick(p.ID)}
            delay={(currentDelay += 200)}
            show={this.props.show}
          />
        ))}
      </React.Fragment>
    );
  }

  render() {
    return (
      <section className="section">
        <div className="container">
          <div className="columns is-centered is-multiline ">
            {this.renderCards()}
          </div>
        </div>
      </section>
    );
  }
}
