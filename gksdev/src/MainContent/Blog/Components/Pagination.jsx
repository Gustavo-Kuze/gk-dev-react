import React, { Component } from 'react';

export default class Pagination extends Component {
  // eslint-disable-next-line react/destructuring-assignment
  createPrevPageHref = () => `/blog/${this.props.getCurrentPage() - 1}`;

  // eslint-disable-next-line react/destructuring-assignment
  createNextPageHref = () => `/blog/${this.props.getCurrentPage() + 1}`;

  setNextButtonBehavior = isCurrentPageTheLast => {
    const nextButton = document.getElementsByClassName('pagination-next')[0];
    if (isCurrentPageTheLast) {
      nextButton.setAttribute('disabled', ``);
      nextButton.setAttribute(
        'title',
        `Você já chegou na ultima página, não tem como avançar mais!`,
      );
    } else {
      nextButton.setAttribute(
        'href',
        this.createNextPageHref() + window.location.search,
      );
    }
  };

  setPrevButtonBehavior = isCurrentPageTheFirst => {
    const prevButton = document.getElementsByClassName(
      'pagination-previous',
    )[0];
    if (isCurrentPageTheFirst) {
      prevButton.setAttribute('disabled', ``);
      prevButton.setAttribute(
        'title',
        `Não tem nenhuma página antes da primeira!`,
      );
    } else {
      prevButton.setAttribute(
        'href',
        this.createPrevPageHref() + window.location.search,
      );
    }
  };

  componentDidUpdate = () => {
    const { currentPage, pages } = this.props;

    const isCurrentPageTheFirst = currentPage === 1;
    const isCurrentPageTheLast = currentPage === pages[pages.length - 1].num;
    this.setPrevButtonBehavior(isCurrentPageTheFirst);
    this.setNextButtonBehavior(isCurrentPageTheLast);
  };

  renderPages = () => {
    const { pages, currentPage } = this.props;
    return pages.map(p => (
      <li key={p.num}>
        <a
          href={p.href + window.location.search}
          className={`pagination-link ${
            p.num === currentPage ? 'is-current' : ''
          }`}
          aria-label={`Página ${p.num}`}
        >
          {p.num}
        </a>
      </li>
    ));
  };

  render() {
    return (
      <section className="section">
        <div className="container">
          <div className="columns is-centered is-multiline">
            <div className="column">
              <nav
                className="pagination is-primary"
                role="navigation"
                aria-label="pagination"
              >
                <a href="/blog/1" className="pagination-previous">
                  Voltar
                </a>
                <a href="/blog/1" className="pagination-next">
                  Avançar
                </a>
                <ul className="pagination-list">{this.renderPages()}</ul>
              </nav>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
