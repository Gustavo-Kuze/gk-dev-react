import './css/Post.css';
import React from 'react';
import h2p from 'html2plaintext';
import { Transition, animated } from 'react-spring';
import {
  styleWordpressResponse,
  formatResponseDate,
} from '../Utils/AfterResponse';

import ShareButtons from './ShareButtons';
import { getBlogUrlOnly } from '../../Base/Utils/Browser';
import If from '../../Base/Components/If';

export default class Post extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      categories: {},
    };
  }

  render() {
    const { show: shouldShow, delay, categories: propsCategories } = this.props;
    return (
      <Transition
        native
        items={shouldShow}
        from={{ opacity: 0 }}
        enter={{ opacity: 1 }}
        leave={{ opacity: 0 }}
        config={{ duration: 300, delay: delay || 300 }}
        onRest={() => {
          try {
            styleWordpressResponse();
            this.setState({ categories: propsCategories });
          } catch (err) {
            console.log(err);
          }
        }}
      >
        {show =>
          show &&
          (props => {
            const { title, description, date, excerpt, content } = this.props;
            const { categories } = this.state;
            return (
              <animated.div
                id="post-container"
                className="container"
                style={props}
              >
                <div className="columns is-centered is-multiline ">
                  <div className="column is-two-thirds ">
                    <h1 id="post-title" className="title">
                      {h2p(title)}
                    </h1>
                    <h2 id="post-subtitle" className="subtitle">
                      {h2p(excerpt)}
                    </h2>
                    <time className="has-text-info">
                      {formatResponseDate(date)}
                    </time>
                    <If condition={categories}>
                      <p>
                        Categorias:{' '}
                        {categories
                          ? Object.keys(categories).map(cat => (
                              <span
                                key={cat}
                                className="has-text-success"
                                style={{ marginLeft: '3px' }}
                              >{`${cat};`}</span>
                            ))
                          : ''}
                      </p>
                    </If>
                    <hr />
                  </div>
                </div>
                <div className="columns is-centered is-multiline ">
                  <div
                    id="post-content-container"
                    className="column is-two-thirds content"
                    dangerouslySetInnerHTML={{ __html: content }}
                  />
                </div>
                <hr />
                <div className="columns is-centered is-multiline is-mobile">
                  <div className="column is-half is-one-quarter">
                    <h3 className="is-size-5">
                      Gostou do post? Compartilhe nas redes sociais!
                    </h3>
                  </div>
                </div>
                <ShareButtons
                  title={title}
                  description={description}
                  URL={getBlogUrlOnly()}
                />
              </animated.div>
            );
          })
        }
      </Transition>
    );
  }
}
