import './css/Blog.css';
import React, { Component } from 'react';
import PageTemplate from '../../Base/Components/PageTemplate';
import Pagination from './Pagination';
import PostsList from './PostsList';
import Post from './Post';
import If from '../../Base/Components/If';
import Loader from '../../Base/Components/Loader';
import {
  fetchPosts,
  fetchPostById,
  fetchPostBySlug,
  fetchCategories,
} from '../Utils/Api';
import {
  getCurrentPage,
  getPathNamePartsLength,
  getDecodedSearchParameters,
} from '../../Base/Utils/Browser';
import { countPagesByPosts } from '../Utils/Math';
import SearchModal from './SearchModal';
import { SELECT_CATEGORY_DEFAULT } from '../Utils/Consts';

let isUpToDate = false;
let apiResponseWasEmpty = false;
let arePostsEmpty = true;

export default class Blog extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isUpToDate: false,
      posts: [],
      postsCount: 0,
      pages: [{ num: 1, href: '1' }],
      offset: 0,
      currentPage: 1,
      currentPost: {},
      isReadingMode: false,
      category: SELECT_CATEGORY_DEFAULT,
      search: '',
      categories: [{ name: SELECT_CATEGORY_DEFAULT }],
    };
  }

  componentDidMount() {
    this.renderOptions();
    this.fillState(true).then(() => {
      if (getPathNamePartsLength() >= 4) {
        this.setCurrentPostBySlug(
          window.location.pathname.split('/').slice(-1)[0],
        );
      }

      window.addEventListener('popstate', this.handleUrlChanged);
    });
  }

  componentDidUpdate() {
    this.fillState();

    const curPage = getCurrentPage();
    const { search, category, pages, isReadingMode } = this.state;

    if (
      search === '' &&
      category === SELECT_CATEGORY_DEFAULT &&
      !apiResponseWasEmpty &&
      isUpToDate &&
      curPage > pages.length
    ) {
      this.setStatePortion({ currentPage: curPage });
      window.location.pathname = '/blog/1';
    }

    if (getPathNamePartsLength() >= 4) {
      if (!isReadingMode) {
        this.setStatePortion({
          isReadingMode: true,
          search: '',
          category: SELECT_CATEGORY_DEFAULT,
        });
      }
    }

    if (getPathNamePartsLength() < 4) {
      if (isReadingMode) {
        this.setStatePortion({ isReadingMode: false });
      }
    }
  }

  setStatePortion = (portion = {}) => {
    this.setState({ ...portion });
  };

  handlePostClick = (id = 1) => {
    fetchPostById(id).then(json => {
      this.setStatePortion({ currentPost: json });
    });
  };

  setCurrentPostBySlug = slug => {
    fetchPostBySlug(slug).then(json => {
      this.setStatePortion({ currentPost: json, isReadingMode: true });
    });
  };

  fetchPostsAndFillState = isUpdating => {
    const { search, category, postsCount } = this.state;
    return fetchPosts(search, category).then(json => {
      apiResponseWasEmpty = json.found === 0;
      const searchParameters = getDecodedSearchParameters();
      let updatingObject = {};

      if (!isUpdating) {
        const pagesCount = countPagesByPosts(json.found);
        const pages = [];
        for (let i = 1; i <= pagesCount; i += 1) {
          pages[i - 1] = { num: i, href: `/blog/${i}` };
        }
        updatingObject = { pages };
      } else {
        updatingObject = { isUpToDate: true };
        isUpToDate = true;
      }

      this.setStatePortion({
        posts: json.posts,
        postsCount: !isUpdating ? json.found : postsCount,
        currentPage: getCurrentPage(),
        isReadingMode: false,
        category: searchParameters.category || SELECT_CATEGORY_DEFAULT,
        search: searchParameters.search || '',
        ...updatingObject,
      });
    });
  };

  fillState = () => {
    const { posts } = this.state;
    arePostsEmpty = posts.length <= 0;

    if (arePostsEmpty && !apiResponseWasEmpty) {
      return this.fetchPostsAndFillState(false);
    }
    if (!isUpToDate && !apiResponseWasEmpty) {
      return this.fetchPostsAndFillState(true);
    }
    return new Promise(resolve => resolve());
  };

  handleUrlChanged = () => {
    const { isReadingMode } = this.state;
    if (!isReadingMode) {
      isUpToDate = false;
      this.setStatePortion({
        isUpToDate: false,
      });
    }
  };

  handleSearchChange = e => {
    this.setStatePortion({
      search: e.target.value,
    });
  };

  handleCategoryChange = e => {
    this.setStatePortion({
      category: e.target.value,
    });
  };

  handleOk = () => {
    const { category, search } = this.state;
    const queryCategory = `${
      category !== SELECT_CATEGORY_DEFAULT ? `category=${category}` : ''
    }`;
    const querySearch = `${search !== '' ? `search=${search}` : ''}`;

    window.location.search = `${queryCategory}${
      queryCategory !== '' && querySearch !== '' ? '&' : ''
    }${querySearch}`;
    apiResponseWasEmpty = false;
    isUpToDate = false;
    this.setStatePortion({
      posts: [],
      isUpToDate: false,
    });
  };

  clearSearch = () => {
    this.setStatePortion({ search: '' });
  };

  renderOptions = () => {
    fetchCategories().then(json => {
      this.setStatePortion({ categories: json.categories });
    });
  };

  render() {
    const {
      isReadingMode,
      categories,
      search,
      pages,
      posts,
      currentPost,
      currentPage,
    } = this.state;
    return (
      <PageTemplate>
        <div className="container is-fluid">
          <div className="columns is-multiline is-centered" />
          <div className="column is-12">
            <If condition={!isReadingMode}>
              <button
                className={`button 
                is-primary is-outlined is-rounded is-medium is-inverted `}
                onClick={() => {
                  const modal = document.querySelectorAll('[modal-message]')[0];
                  modal.classList.add('is-active');
                }}
                type="button"
              >
                <i className="fas fa-search" />
              </button>
            </If>
            <If condition={isReadingMode}>
              <button
                className={`button 
                is-primary is-outlined is-rounded is-medium is-inverted`}
                onClick={() => {
                  window.history.back();
                }}
                type="button"
              >
                <i className="fas fa-arrow-left" />
              </button>
            </If>
            <SearchModal
              handleOk={this.handleOk}
              handleSearchChange={this.handleSearchChange}
              search={search}
              clearSearch={this.clearSearch}
              handleCategoryChange={this.handleCategoryChange}
              categories={categories}
            />
            <If condition={!isReadingMode && posts.length > 0}>
              <PostsList
                posts={posts}
                currentPage={currentPage}
                handlePostClick={this.handlePostClick}
                show={!isReadingMode && posts.length > 0}
              />
            </If>
            <If condition={isReadingMode}>
              <Post
                img={currentPost.featured_image}
                content={currentPost.content}
                title={currentPost.title}
                date={currentPost.modified}
                URL={currentPost.URL}
                excerpt={currentPost.excerpt}
                show={isReadingMode}
                categories={currentPost.categories}
              />
            </If>
            <Loader load={!isUpToDate && !apiResponseWasEmpty} />
            <If condition={!isReadingMode && posts.length > 0}>
              <Pagination
                pages={pages}
                currentPage={currentPage}
                getCurrentPage={getCurrentPage}
              />
            </If>
          </div>
        </div>
      </PageTemplate>
    );
  }
}
