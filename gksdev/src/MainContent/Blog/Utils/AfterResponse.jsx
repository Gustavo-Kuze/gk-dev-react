import moment from 'moment';
import h2p from 'html2plaintext';
import 'highlight.js/styles/github.css';
import hljs from 'highlight.js/lib/';

const styleTiledGalleries = () => {
  const tiledGalleries = document.querySelectorAll('.tiled-gallery');

  tiledGalleries.forEach(gallery => {
    const galleryRow = gallery.children[0];
    if (!galleryRow.classList.contains('columns')) {
      galleryRow.classList.add(
        'columns',
        'is-multiline',
        'is-centered',
        'is-gapless',
        'is-offset-3',
      );
      gallery.style.marginTop = gallery.style.marginBottom = '270px';
      galleryRow.removeAttribute('style');

      Array.from(galleryRow.children).forEach(galleryItem => {
        galleryItem.classList.add('column');
        galleryItem.removeAttribute('style');
      });
    }
  });
};

const styleTiledGalerriesImgs = () => {
  Array.from(document.querySelectorAll('.tiled-gallery-item > a')).forEach(
    a => {
      a.removeAttribute('href');
    },
  );

  Array.from(
    document.querySelectorAll('.tiled-gallery-item > a > img'),
  ).forEach(child => {
    child.removeAttribute('title');
  });
};

const styleWpCaptions = () => {
  const wpCaptions = document.querySelectorAll(
    '.tiled-gallery-caption, .wp-caption-text',
  );

  wpCaptions.forEach(caption => {
    caption.innerHTML = '';
    caption.classList.remove('tiled-gallery-caption');
  });
};

const styleSubtitle = () => {
  const subtitle = document.getElementById('post-subtitle');
  const subtitleOldText = subtitle.innerText;
  subtitle.innerText = h2p(subtitleOldText);
};

const styleImages = () => {
  const images = document.querySelectorAll(
    `#post-content-container :not(.tiled-gallery-item) > :not(a) > img`,
  );

  images.forEach(i => {
    if (!i.classList.contains('image')) {
      i.classList.add('image');
      i.removeAttribute('width');
      i.removeAttribute('height');
      i.style.marginTop = i.style.marginBottom = '50px';
    }
  });
};

const stylePostContentContainer = () => {
  Array.from(document.querySelectorAll('#post-content-container div')).forEach(
    child => {
      child.removeAttribute('style');
      child.removeAttribute('width');
      child.removeAttribute('height');
    },
  );

  Array.from(
    document.querySelectorAll(
      '#post-content-container p, #post-content-container h1, #post-content-container h1',
    ),
  ).forEach(child => {
    child.style.marginTop = child.style.marginBottom = '50px';
  });
};

const styleCodePres = () => {
  Array.from(document.querySelectorAll('#post-content-container pre')).forEach(
    pre => {
      //pre.classList.add('is-size-7');
      //pre.style.backgroundColor = '#222'
      hljs.highlightBlock(pre);
    },
  );
};

const styleWordpressResponse = () => {
  styleTiledGalleries();
  styleTiledGalerriesImgs();
  styleWpCaptions();
  styleSubtitle();
  styleImages();
  styleCodePres();
  stylePostContentContainer();
};

const formatResponseDate = date => {
  return moment(date).format('DD-MM-YYYY, LT');
};

export { styleWordpressResponse, formatResponseDate };
