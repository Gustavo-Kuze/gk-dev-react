const countPagesByPosts = postsCount => {
  let extraPage = postsCount % 6 > 0 ? 1 : 0;
  let count = postsCount / 6 + extraPage;
  return count;
};

export { countPagesByPosts };
