import { getCurrentPage } from '../../Base/Utils/Browser';

const baseUrl =
  'https://public-api.wordpress.com/rest/v1/sites/gustavokuze.wordpress.com';
const postsUrl = baseUrl + '/posts';

const fetchPosts = () => {
  let fetchUrl = `${postsUrl}?offset=${(getCurrentPage() - 1) *
    6}&number=6&fields=ID,title,excerpt,modified,featured_image,found,slug,URL,categories${
    window.location.search !== ''
      ? '&' + window.location.search.replace('?', '')
      : ''
  }`;
  return fetch(fetchUrl).then(response => response.json());
};

const fetchCategories = () => {
  let fetchUrl = `${baseUrl}/categories?fields=name`;
  return fetch(fetchUrl).then(response => response.json());
};

const fetchPostById = (id = 1) => {
  return fetch(`${postsUrl}/${id}`).then(response => response.json());
};

const fetchPostBySlug = (slug = '') => {
  return fetch(`${postsUrl}/slug:${slug}`).then(response => response.json());
};

export { fetchPosts, fetchPostById, fetchPostBySlug, fetchCategories };
