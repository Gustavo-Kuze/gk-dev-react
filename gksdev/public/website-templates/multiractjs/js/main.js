
(function (j) {
    $ = j

    enterGetFilesMode()

    $('[data-toggle="tooltip"]').tooltip()

    if(isThisDeviceTouch()){
        disableFixedBackImg()
    }

})(jQuery)

function isThisDeviceTouch(){
    return (('ontouchstart') in document.documentElement)
}

function disableFixedBackImg(){
    $('body').css('background-attachment', 'scroll')
    $('body').css('background-repeat', 'repeat-y')
}


$("#btn-app, #btn-about-proj, #index-brand").click(function() {
    scrollToTop()
    return false;
});

function scrollToTop(){
    $("html, body").animate({ scrollTop: 0 }, "slow");
}

$('#btn-load-more').click(function () {
   enterGetFilesMode()
})


function copyToClipBoard(tessOutput) {
    let $temp = $('<input>') 
    $('body').append($temp)
    $temp.val(tessOutput.text()).select()
    document.execCommand('copy')
    $temp.remove()
    showCopiedAlert()
}

function showCopiedAlert() {
    $('[tess-alert-container]').load('./components/alert-text-copied.html', ()=>{
        setTimeout(() => {
            $('#alert-text-copied').fadeOut()
        }, 1800);
    })
}


$('#drop-zone').on(
    'dragover',
    function (e) {
        e.preventDefault();
        e.stopPropagation();
    }
)
$('#drop-zone').on(
    'dragenter',
    function (e) {
        e.preventDefault();
        e.stopPropagation();
    }
)
$('#drop-zone').on(
    'drop',
    function (e) {
        if (e.originalEvent.dataTransfer) {
            if (e.originalEvent.dataTransfer.files.length) {
                e.preventDefault();
                e.stopPropagation();
                appendTessItems(e.originalEvent.dataTransfer.files, true)
            }
        }
    }
);

function startTessRecog(row, language = 'por') {
    const i = $(row).attr('tess-item')[0]
    
    const img = $(`[tess-img="${i}"]`)[0]
    
    const progressBarContainer = $(`[prg-bar-container="${i}"]`)[0]
    const progressBar = $(`[prg-bar="${i}"]`)[0]

    $(progressBarContainer).fadeIn()
    Tesseract.recognize(img, { lang: language })
        .progress((message) => {
            
            let progressText = 'Inicializando a API: '
            if (message.status === 'recognizing text') {
             progressText = 'Reconhecendo o texto: '   
            }

            const percent = Math.floor(message.progress * 100)
            $(progressBar).css('width', `${percent}%`).html(`${progressText} ${percent}%`)
        })
        .catch(err => console.error(err))
       
        .finally(resultOrError => {
            const p = $(`[tess-p="${i}"]`)[0]
            $(p).html(`${resultOrError.text}`)
            $(progressBarContainer).fadeOut()
        })
}

$('#input-image').change(function () {
    appendTessItems(document.querySelector('#input-image').files, false)
})


function enterGetFilesMode(){
    $('#tess-items-container').empty()
    $('#drop-zone').fadeIn()
    $('#btn-load-more').fadeOut()
    $('#btn-search').fadeIn()
    $('.tess-p-escolha').fadeIn()
    $('#select-lang').fadeIn()
}

function enterRecogMode(){
    $('#tess-items-container').empty()
    $('#drop-zone').fadeOut()
    $('#btn-load-more').fadeIn()
    $('#btn-search').fadeOut()
    $('.tess-p-escolha').fadeOut()
    $('#select-lang').fadeOut()
}

function appendTessItems(files, isDraggingFiles) {

    enterRecogMode()

    if (isDraggingFiles) {

        for (let index = 0; index < files.length; ++index) {
            const file = files[index]
            let reader = new FileReader()
            reader.onloadend = () => {
                appendSingleTessItem(reader.result, index)
            }
            reader.readAsDataURL(file)
        }
    } else {
        for (let index = 0; index < files.length; ++index) {
            const file = files[index];
            let imagePath = URL.createObjectURL(file)
            appendSingleTessItem(imagePath, index)
        }
    }

}

function appendSingleTessItem(src, i) {

    const $container = $('#tess-items-container')
    let $divItem = $('<div tess-item class="row py-2 border border-secondary bg-secondary">')
        .load('./components/tess-item.html', () => {
            setTessItemIndexes($divItem, i)

            const img = $(`[tess-img=${i}]`)[0]

            $(img).attr('src', src)
            img.addEventListener('load', () => {
                const row = $(`[tess-item="${i}"]`)[0]

                const language = $('#select-lang').find(':selected').val()
                startTessRecog(row, language)
            })


            $(`[tess-a="${i}"]`).click(() => {
                copyToClipBoard($(`[tess-p="${i}"]`))
            })

        })

    $container.append($divItem).append('<div class="my-3">')
}

function setTessItemIndexes($tessItemsContainer, i) {
    $tessItemsContainer.attr('tess-item', i)
    $tessItemsContainer.find(`img`).attr('tess-img', i)
    $tessItemsContainer.find(`p`).attr('tess-p', i)
    $tessItemsContainer.find(`a`).attr('tess-a', i)
    $tessItemsContainer.find(`[prg-bar]`).attr('prg-bar', i)
    $tessItemsContainer.find(`[prg-bar-container]`).attr('prg-bar-container', i)
}


$(window).on('resize', resizeHeader);
$(window).on('load', resizeHeader);


function resizeHeader() {
    var win = $(this);
    if (win.width() < 500) {
  
      $('#tess-title').removeClass('display-1').addClass('display-4')
      $('#tess-subtitle-spacer').show()
    } else {
        $('#tess-title').removeClass('display-4').addClass('display-1')
        $('#tess-subtitle-spacer').hide()
    }
  }






