
const secsIds = ['sec-inicio', 'sec-belezas', 'sec-pessoas', 'sec-clima', 'sec-rotas']
let curSec = 0

const getNextSec = () => {
    const nextSec = curSec + 1
    curSec = (nextSec > secsIds.length - 1) ? 0 : nextSec
    return curSec
}

const getPrevSec = () => {
    const prevSec = curSec - 1
    curSec = (prevSec < 0) ? secsIds.length - 1 : prevSec
    return curSec
}

$(document).scroll(() => {
    if (isSectionFocused('#sec-inicio')) {
        $('li').removeClass('active')
        $('#ni-inicio').addClass('active')
    } else if (isSectionFocused('#sec-belezas')) {
        $('li').removeClass('active')
        $('#ni-belezas').addClass('active')
    }
    else if (isSectionFocused('#sec-pessoas')) {
        $('li').removeClass('active')
        $('#ni-pessoas').addClass('active')
    }
    else if (isSectionFocused('#sec-clima')) {
        $('li').removeClass('active')
        $('#ni-clima').addClass('active')
    }
    else if (isSectionFocused('#sec-rotas')) {
        $('li').removeClass('active')
        $('#ni-rotas').addClass('active')
    }
})

$("#btn-scroll-next").click(function (e) {
    e.preventDefault()
    scrollToElem(`#${secsIds[getNextSec()]}`)
});

$("#btn-scroll-prev").click(function (e) {
    e.preventDefault()
    scrollToElem(`#${secsIds[getPrevSec()]}`)
});

const scrollToElem = (elem) => {
    $([document.documentElement, document.body]).animate({
        scrollTop: $(elem).offset().top
    }, 300);
}

const isSectionFocused = ( sec ) =>
    $(document).scrollTop() >= $(sec).offset().top &&
    $(document).scrollTop() <= $(sec).offset().top + $(sec).height()


$("#ni-inicio a").click(function (e) {
    e.preventDefault()
    scrollToElem('#sec-inicio')
});

$("#ni-belezas a").click(function (e) {
    e.preventDefault()
    scrollToElem('#sec-belezas')
});

$("#ni-pessoas a").click(function (e) {
    e.preventDefault()
    scrollToElem('#sec-pessoas')
});

$("#ni-clima a").click(function (e) {
    e.preventDefault()
    scrollToElem('#sec-clima')
});

$("#ni-rotas a").click(function (e) {
    e.preventDefault()
    scrollToElem('#sec-rotas')
});
