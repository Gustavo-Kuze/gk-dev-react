/* Flipbook methods */
$("#flipbook").turn({
    height: 750,
    display: "double",
    autoCenter: true,
    acceleration: true,
    duration: 400
});

$('#flipbook').bind("turned", () => {
    if (isBookDoubleDisplay()) {
        flipHardPages()
    }
})
/* Flipbook methods */

/* Book behavior */

function flipHardPages() {
    if (turnJs('page') <= 1) {
        turnJs('page', 2)

        try {
            turnJs('page', 2)
        } catch (error) {
            console.log('Error trying to turn the page automatically')
            console.log(error)
        }
    }
    if (turnJs('page') >= turnJs('pages')) {
        try {
            turnJs('page', (turnJs('pages') - 1))
        } catch (error) {
            console.log('Error trying to turn the page automatically')
            console.log(error)
        }
    }
}

function adjustBookSize() {
    let width = (window.innerWidth - (window.innerWidth / 7))
    turnJs('size', width, 750)
    $('#flipbook').css('left', (width / 13))
}

function loadPosts() {
    $('#capa').load('/website-templates/tl/components/insta/capa.html')
    let posts = [
        newPage('/website-templates/tl/components/insta/insta-post-lidos.html'),
        newPage('/website-templates/tl/components/insta/insta-post-aia.html'),
        newPage('/website-templates/tl/components/insta/insta-miniaturista.html'),
        newPage('/website-templates/tl/components/insta/insta-morro.html'),
        newPage('/website-templates/tl/components/insta/insta-tbr.html'),
        newPage('/website-templates/tl/components/insta/insta-dicas-cuidados.html')
    ]

    for (const post of posts) {
        addPage(post)
    }
}


/* Book Behavior */

/* Utils */

function isBookDoubleDisplay() {
    return turnJs('display') === 'double'
}

function newPage(url) {
    return ($('<div>').load(url))
}

function addPage(page) {
    turnJs('addPage', page)
}

function addPageAt(page, index) {
    turnJs('addPage', page, index)
}

function removePage(index) {
    return turnJs('removePage', index)
}

function turnJs(methodName, ...parameters) {
    return $('#flipbook').turn(methodName, ...parameters)
}


function removeAllPages() {
    try {
        for (let i = 1; i <= turnJs('pages') + 10; i++) {
            try {
                turnJs('removePage', turnJs('pages'))
            } catch (err) {
                console.log('Error while removing a page:')
                console.log(err)
            }
        }
    } catch (error) {
        console.log('caralho viu')
    }
}
/* Utils */



window.addEventListener('resize', () => {
    adjustBookSize()

    if (window.innerWidth <= 795) {
        if (isBookDoubleDisplay()) {
            turnJs('display', 'single')
            turnJs('removePage', turnJs('pages'))
            turnJs('removePage', turnJs('pages'))
            turnJs('removePage', 1)
            turnJs('page', 1)
        }
    } else {
        if (!isBookDoubleDisplay()) {
            turnJs('display', 'double')
            removeAllPages()
            addPage($('<div class="hard">'))
            addPage($('<div id="capa" class="hard">'))
            loadPosts()
            addPage($('<div class="hard">'))
            addPage($('<div class="hard">'))
            turnJs('page', 2)
        }
    }
})


function initFlip() {
    loadPosts()

    if (window.innerWidth <= 795) {
        turnJs('display', 'single')
        removePage(1)
        turnJs('page', 1)
    } else {
        turnJs('display', 'double')
        addPage($('<div class="hard">'))
        addPage($('<div class="hard">'))
        turnJs('page', 2)
    }

    adjustBookSize()
}

