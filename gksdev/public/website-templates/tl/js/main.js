
document.addEventListener('scroll', () => {
    $('#header-background').css('background-position-y',
        function () { return `${window.pageYOffset * -0.2}px` })
    // console.log(window.pageYOffset * 0.7)
    // console.log(window.pageYOffset)
    // if(window.pageYOffset > 750){
    //     $('#menu-section').addClass('fixed-top')
    // }else if(window.pageYOffset < 372){
    //     $('#menu-section').removeClass('fixed-top')
    // }
})

function loadFlipbook() {
    $('#book').html($('<div class="container-fluid"><div class="col-12">')).load('/website-templates/tl/components/turnjs/book.html', () => {
        initFlip()

    })
}

let oldRandIndex = 0

function getRandInt(min, max) {
    let newRandIndex = Math.floor(Math.random() * (max - min + 1)) + min;

    if (newRandIndex === oldRandIndex) {
        oldRandIndex = newRandIndex
        newRandIndex = getRandInt(min, max)
    }

    oldRandIndex = newRandIndex
    return newRandIndex
}

function changeQuote() {
    const $headerSub = $('#header-subtitle')
    const $headerBackground = $('#header-background')
    const quoteIndex = getRandInt(0, quotes.length - 1)
    $headerSub.html((quotes[quoteIndex]))

    const subSize = parseInt($headerSub.css('height'))
    $headerBackground.css('height', `${subSize + 300}px`)
}

$(document).ready(() => {
    loadFlipbook()
    changeQuote()
    setInterval(() => {
        const headerSub = $('#header-subtitle')
        headerSub.fadeOut("slow", () => {
            changeQuote()
            headerSub.fadeIn("slow")
        })

    }, 8000)
})



const quotes = [
    `❝Ele se fazia perguntas, que ficavam sem respostas. 
     De que servia matar? Por que existiam homens maus no mundo?
      Ele não era capaz de arranhar um colega, de jogar pedras nos cachorros; 
     não fazia malvadeza com os bichos nem com as pessoas. 
     Doía-lhe ver os outros sofrerem. Tinha horror ao sangue.
      Como era então que havia no mundo gente que tinha coragem de apunhalar
      honrados lavradores como o pobre Ângelo?❞ - Olhai os Lírios do campo`,

    `❝Lembrou-se duma frase de Olívia: 'A bondade não é uma virtude passiva'.
       Como era fácil ser mau e como era ainda mil vezes mais fácil ser indiferente!
       A roda da vida girava e no fim de tudo estava a morte, o silêncio, o aniquilamento.❞ - Olhai os Lírios do Campo`,

    `❝Eugênio chegava mais uma vez à mesma conclusão. A gentileza podia melhorar o mundo.
    Se os homens cultivassem a gentileza seria possível atenuar um pouco tudo quanto a vida 
    tinha de áspero e brutal❞ - Olhai os Lírios do Campo`,

    `❝Havia muita coisa a fazer no mundo: proporcionar uma vida melhor àquela gente, por exemplo.
    Não se devia fazer isso com revoluções, porque a violência gera a violência e seus frutos 
    são sempre perigosos. Os homens viviam demasiadamente preocupados com palavras, pulavam ao
     redor delas e se esqueciam dos fatos. E os fatos continuavam a bater-lhes na cara.❞ - Olhai os Lírios do Campo`,

    `❝toda vez que você diz para sua filha que grita com ela por amor você a ensina a confundir
    raiva com carinho o que parece uma boa ideia até que ela cresce confiando em homens
     violentos porque eles são tão parecidos com você ~aos pais que têm filhas❞ - Outros jeitos de usar a boca`,

    `"Eu não gostava de ler até o dia em que tive medo de não poder mais. Ninguém ama respirar." - O Sol é para todos`,

    `"Ainda que tenhamos perdido antes mesmo de começar, não significa que não devemos tentar" - O sol é para todos`
]