# GK-Dev-React

Este é o repositório do meu Site pessoal!

Ele é feito totalmente em React e faz integração com a API do Wordpress para suprir o blog.

## Tecnologias usadas
A construção do meu site não seria possível sem as seguintes tecnologias:

Estilo | Comportamento | Produtividade | Desenvolvimento
--- | --- | --- | --- |
[Bulma](https://bulma.io/) | [React.js](https://reactjs.org/) | [Moment.js](https://momentjs.com/) | [Visual Studio Code](https://code.visualstudio.com/)
[React Spring](https://react-spring.surge.sh/) | [React-Share](https://www.npmjs.com/package/react-share) | [Html2PlainText](https://www.npmjs.com/package/html2plaintext) | [Webpack](https://webpack.js.org/)
 [FontAwesome](https://origin.fontawesome.com/)| [Typed.js](https://github.com/mattboldt/typed.js/) | [Highlight.js](https://highlightjs.org/) | [Node-Sass](https://www.npmjs.com/package/node-sass)
 | | [Wordpress](https://wordpress.com/)| [Netlify-Cli](https://www.netlify.com/docs/cli/)|
